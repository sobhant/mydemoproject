<?php
namespace Yoda\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Yoda\EventBundle\Entity\Event;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadEvents implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!
        $sobhan = $manager->getRepository('UserBundle:User')
        ->findOneByUsernameOrEmail('sobhan');

        $event1 = new Event();
        $event1->setName('Hello');
        $event1->setLocation('bhubaneswar');
        $event1->setTime(new \DateTime('tomorrow noon'));
        $event1->setDetails('Sobhan Surprises');

        $event2 = new Event();
        $event2->setName('sobhan thakur');
        $event2->setLocation('bhubaneswar');
        $event2->setTime(new \DateTime('tomorrow noon'));
        $event2->setDetails('Sobhan Surprises');

        $event1->setOwner($sobhan);
        $event2->setOwner($sobhan);

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
