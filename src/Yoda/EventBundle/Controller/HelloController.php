<?php

namespace Event\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
class DefaultController extends Controller
{
	/**
	* @Route("/home",name="app_index_route")
	*/
    public function helloAction()
    {
        return $this->render('EventBundle:Hello:hello.html.twig');
    }
}