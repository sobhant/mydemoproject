<?php
namespace Yoda\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Yoda\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadUsers implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;
    /**
    * @var ContainerInterface
    */
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!
        /**
        @ {@inheritDoc}
        */
       $user = new User();
       $user->setUsername('sobhan');
       $user->setEmail('sobhan@gmail.com');
       $user->setPassword($this->encodePassword($user,'password'));
       $manager->persist($user);

       $admin = new User();
       $admin->setUsername('arpan');
       $admin->setEmail('arpan@gmail.com');
       $admin->setPassword($this->encodePassword($admin,'password123'));
       $admin->setRoles(array('USER_ADMIN'));
       $manager->persist($admin);

        $manager->flush();
    }
    private function encodePassword(User $user,$plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        return $encoder->encodePassword($plainPassword,$user->getSalt());
    }
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

     public function getOrder()
    {
        return 10;
    }
}
