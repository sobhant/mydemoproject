<?php

namespace Yoda\UserBundle\Controller;

use Yoda\EventBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SecurityController extends Controller
{
	/**
	* @Route("/login",name="login_form");
	* @Template
	*/
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);

        // get the login error if there is one
        // if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
        //     $error = $request->attributes->get(
        //         SecurityContextInterface::AUTHENTICATION_ERROR
        //     );
        // } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
        //     $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        //     $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        // } else {
        //     $error = '';
        // }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            );
        
    }

    /**
	* @Route("/login_check",name="login_check")
    */
    public function loginCheckAction()
    {

    }

    /**
	* @Route("/logout",name="logout")
    */
    public function logoutAction()
    {

    }
}
