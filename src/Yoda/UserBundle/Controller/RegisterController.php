<?php 
namespace Yoda\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Yoda\UserBundle\Entity\User;
use Yoda\UserBundle\Form\RegisterFormType;
use Yoda\EventBundle\Controller\Controller;

class RegisterController extends Controller
{
 /**
 * @Route("/register", name="user_register")
 * @Template
 */
    public function registerAction(Request $request)
    {
	    $defaultUser = new User();
	    $defaultUser->setUsername('Foo');

	    $form = $this->createForm(new RegisterFormType(), $defaultUser);


        $form->handleRequest($request);
	if ($form->isSubmitted() && $form->isValid()) {
    $data = $form->getData();

    $user = new User();
    $user->setUsername($data['username']);
    $user->setEmail($data['email']);
    $user->setPassword($this->encodePassword($user, $data['password']));


    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

     $url = $this->generateUrl('event');
     return $this->redirect($url);
	}
    	return array('form' => $form->createView());
    }

    private function encodePassword(User $user, $plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')
            ->getEncoder($user)
        ;

        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }

    private function authenticateUser(User $user)
    {
        $providerKey = 'secured_area'; // your firewall name
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->getSecurityContext()->setToken($token);
    }
}