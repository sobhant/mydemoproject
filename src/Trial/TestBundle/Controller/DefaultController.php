<?php

namespace Trial\TestBundle\Controller;

use Trial\TestBundle\Entity\Articles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	/**
	* @Route("/create_article",name="create_article_route")
	*/
    public function createAction()
    {
        $article = new Articles();
        $article->setTitle('symfony');
        $article->setDescription('symfony Series');
        $article->setContent('Contents');

	    $em = $this->getDoctrine()->getManager();
	    $em->persist($article);
	    $em->flush();

         return new Response('Saved new article with id = '.$article->getId());

    }

    /**
	* @Route("/show_article/{idArticle}",name="show_article_route")
	*/

	public function showAction($idArticle)
	{
		$em = $this->getDoctrine()->getManager();
		$articleRepository = $em->getRepository('TestBundle:Articles');
		$article = $articleRepository->find($idArticle);

		if(is_null($article))
		{
			throw $this->createNotFoundException('No Article found for this ID');
		}
		return $this->render('TestBundle:Default:article.html.twig',['article' => $article]);
	}


	/**
	* @Route("/update_article/{idArticle}",name="update_article_route")
	*/

	public function updateAction($idArticle)
	{
		$em = $this->getDoctrine()->getManager();
		$articleRepository = $em->getRepository('TestBundle:Articles');
		$article = $articleRepository->find($idArticle);

		if(is_null($article))
		{
			throw $this->createNotFoundException('No Article found for this ID');
		}
		
		$article->setTitle('New Title');

		$em->flush();
		return new Response('Article '.$article->getId().' is updated');
	}

	/**
	* @Route("/delete_article/{idArticle}",name="delete_article_route")
	*/

	public function deleteAction($idArticle)
	{
		$em = $this->getDoctrine()->getManager();
		$articleRepository = $em->getRepository('TestBundle:Articles');
		$article = $articleRepository->find($idArticle);

		if(is_null($article))
		{
			throw $this->createNotFoundException('No Article found for this ID');
		}
		
		$em->remove($article);

		$em->flush();
		return new Response('Article is deleted');
	}
}
