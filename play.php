<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;
use Yoda\EventBundle\Entity\Event;



$loader = require_once __DIR__.'/app/bootstrap.php.cache';
Debug::enable();

require_once __DIR__.'/app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$kernel->boot();

$container = $kernel->getContainer();
$container->enterScope('request');
$container->set('request',$request);


/*
$templating = $container->get('templating');
echo $templating->render(
	'EventBundle:Default:index.html.twig',
	array('name' => 'sobhan','count'=>'5')
);
*/

// $event = new Event();
// $event->setName('sobhan thakur');
// $event->setLocation('bhubaneswar');
// $event->setTime(new \DateTime('tomorrow noon'));
// $event->setDetails('Sobhan Surprises');


use Doctrine\ORM\EntityManager;
$em = $container->get('doctrine')->getManager();

$user = $em
    ->getRepository('UserBundle:User')
    ->findOneBy(array('username' => 'sobhan'))
;

foreach ($user->getEvents() as $event) {
    var_dump($event->getName());
}

// $em->persist($event);
// $em->flush();
